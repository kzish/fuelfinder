#pragma checksum "C:\Users\samaritan\Documents\Visual Studio 2017\projects\rubiem\FuelFinder\AdminPortal\Views\FuelStations\CreateStation.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "88747461c7e948fdd83f5bc5cf74ad51a8f0bad3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_FuelStations_CreateStation), @"mvc.1.0.view", @"/Views/FuelStations/CreateStation.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/FuelStations/CreateStation.cshtml", typeof(AspNetCore.Views_FuelStations_CreateStation))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"88747461c7e948fdd83f5bc5cf74ad51a8f0bad3", @"/Views/FuelStations/CreateStation.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"57218c316b6921e2cd61027a2387edc31a2d9471", @"/Views/_ViewImports.cshtml")]
    public class Views_FuelStations_CreateStation : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("~/FuelStations/CreateStation"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", new global::Microsoft.AspNetCore.Html.HtmlString("post"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Users\samaritan\Documents\Visual Studio 2017\projects\rubiem\FuelFinder\AdminPortal\Views\FuelStations\CreateStation.cshtml"
  
    Layout = "~/Views/Shared/_LayoutMain.cshtml";
    //creates a station

#line default
#line hidden
            BeginContext(83, 220, true);
            WriteLiteral("\r\n\r\n<div class=\"container\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n            <h3>Create A Station</h3>\r\n        </div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n            ");
            EndContext();
            BeginContext(303, 4041, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "88747461c7e948fdd83f5bc5cf74ad51a8f0bad34262", async() => {
                BeginContext(361, 3976, true);
                WriteLiteral(@"

                <div class=""row"">
                    <div class=""col-md-4"">
                        <div class=""form-group"">
                            <input type=""text"" class=""form-control"" name=""StationName"" required>
                            <small class=""form-text text-muted""><span style=""color:red;"">*</span> StationName</small>
                        </div>
                    </div>
                    <div class=""col-md-4"">
                        <div class=""form-group"">
                            <input type=""text"" class=""form-control"" name=""ContactPersonName""  required>
                            <small class=""form-text text-muted""><span style=""color:red;"">*</span> ContactPersonName</small>
                        </div>
                    </div>
                    <div class=""col-md-4"">
                        <div class=""form-group"">
                            <input type=""text"" class=""form-control"" name=""ContactPersonMobile"" required>
                            <s");
                WriteLiteral(@"mall class=""form-text text-muted""><span style=""color:red;"">*</span> ContactPersonMobile</small>
                        </div>
                    </div>
                </div>


                <div class=""row"">
                    <div class=""col-md-4"">
                        <div class=""form-group"">
                            <input type=""text"" class=""form-control"" name=""AddressProvince"" required>
                            <small class=""form-text text-muted""><span style=""color:red;"">*</span> AddressProvince</small>
                        </div>
                    </div>
                    <div class=""col-md-4"">
                        <div class=""form-group"">
                            <input type=""text"" class=""form-control"" name=""AddressCity"" required>
                            <small class=""form-text text-muted""><span style=""color:red;"">*</span> AddressCity</small>
                        </div>
                    </div>
                    <div class=""col-md-4"">
          ");
                WriteLiteral(@"              <div class=""form-group"">
                            <input type=""text"" class=""form-control"" name=""AddressStreet"" required>
                            <small class=""form-text text-muted""><span style=""color:red;"">*</span> AddressStreet</small>
                        </div>
                    </div>
                </div>


                <div class=""row"">
                    <div class=""col-md-4"">
                        <div class=""form-group"">
                            <input type=""text"" class=""form-control"" name=""Latitude"" required>
                            <small class=""form-text text-muted""><span style=""color:red;"">*</span> Latitude</small>
                        </div>
                    </div>
                    <div class=""col-md-4"">
                        <div class=""form-group"">
                            <input type=""text"" class=""form-control"" name=""Longitude"" required>
                            <small class=""form-text text-muted""><span style=""color:re");
                WriteLiteral(@"d;"">*</span> Longitude</small>
                        </div>
                    </div>
                </div>

                <div class=""row"">
                    <div class=""col-md-12"">
                        <div class=""form-group"">
                            <textarea name=""AddressDescription"" class=""form-control"" required placeholder=""...""></textarea>
                            <small class=""form-text text-muted""><span style=""color:red;"">*</span> AddressDescription</small>
                        </div>
                    </div>
                </div>

                <div class=""row"">
                    <div class=""col-md-12"">
                        <div class=""form-group"">
                            <button class=""btn btn-primary"" type=""submit"">Submit</button>
                        </div>
                    </div>
                </div>

            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4344, 40, true);
            WriteLiteral("\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
