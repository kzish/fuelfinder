﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminPortal.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AdminPortal.Controllers
{
    [Route("FuelTanks")]
    [Authorize(Roles = "admin")]
    public class FuelTanksController : Controller
    {
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [Route("AllTanks/{station_id}")]
        public IActionResult AllTanks(int station_id)
        {
            ViewBag.title = "All Tanks";
            var station = db.MFuelStations.Find(station_id);
            ViewBag.station = station;
            return View();
        }


        [Route("ajaxAllTanks/{id}")]
        public IActionResult ajaxAllTanks(int id)
        {
            var tanks = db.MFuelTanks
                 .Include(i => i.EFuelTypeNavigation)
                 .Include(i => i.EFuelCurrencyNavigation)
                 .Where(i => i.FuelStation == id)
                 .ToList();
            ViewBag.tanks = tanks;
            return View();
        }

        [HttpGet("CreateTank/{station_id}")]
        public IActionResult CreateTank(int station_id)
        {
            ViewBag.title = "Create Tank";

            var station = db.MFuelStations.Find(station_id);
            var fuel_types = db.EFuelTypes.ToList();
            var fuel_currencys = db.EFuelCurrency.ToList();

            ViewBag.station = station;
            ViewBag.fuel_currencys = fuel_currencys;
            ViewBag.fuel_types = fuel_types;

            return View();
        }

        [HttpPost("CreateTank")]
        public IActionResult CreateTank(MFuelTanks tank)
        {
            ViewBag.title = "Create Tank";
            try
            {
                db.MFuelTanks.Add(tank);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("AllTanks","FuelTanks",new { station_id = tank.FuelStation});
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllTanks", "FuelTanks", new { station_id = tank.FuelStation });
            }
        }




        [HttpGet("ViewTank/{id}")]
        public IActionResult ViewTank(int id)
        {
            ViewBag.title = "View Tank";
            var tank = db.MFuelTanks
                .Include(i => i.EFuelTypeNavigation)
                .Include(i => i.EFuelCurrencyNavigation)
                .Include(i => i.FuelStationNavigation)
                .Where(i => i.Id == id)
                .FirstOrDefault();

            ViewBag.tank = tank;

            var station = db.MFuelStations.Find(tank.FuelStation);
            var fuel_types = db.EFuelTypes.ToList();
            var fuel_currencys = db.EFuelCurrency.ToList();

            ViewBag.station = station;
            ViewBag.fuel_currencys = fuel_currencys;
            ViewBag.fuel_types = fuel_types;

            return View();
        }

        [HttpPost("UpdateTank")]
        public IActionResult UpdateTank(MFuelTanks tank)
        {
            try
            {
                db.Entry(tank).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("AllTanks", "FuelTanks", new { station_id = tank.FuelStation });
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllTanks", "FuelTanks", new { station_id = tank.FuelStation });
            }
        }

        [HttpPost("DeleteTank")]
        public IActionResult DeleteTank(int id)
        {
            var tank = db.MFuelTanks.Find(id);
            try
            {
                db.MFuelTanks.Remove(tank);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("AllTanks", "FuelTanks", new { station_id = tank.FuelStation });
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllTanks", "FuelTanks", new { station_id = tank.FuelStation });
            }
        }

    }
}
