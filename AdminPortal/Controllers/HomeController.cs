﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace AdminPortal.Controllers
{
    [Route("Home")]
    [Route("")]
    [Authorize(Roles = "admin")]
    public class HomeController : Controller
    {
        [Route("Dashboard")]
        [Route("")]
        public IActionResult Dashboard()
        {
            ViewBag.title = "Dashboard";
            return View();
        }
    }
}
