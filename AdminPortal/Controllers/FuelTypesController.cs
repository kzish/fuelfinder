﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminPortal.Models;
using Microsoft.AspNetCore.Mvc;


namespace AdminPortal.Controllers
{
    [Route("FuelTypes")]
    public class FuelTypesController : Controller
    {

        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [HttpGet("ajaxAllFuelTypes")]
        public IActionResult ajaxAllFuelTypes()
        {
            var fuel_types = db.EFuelTypes.ToList();
            ViewBag.fuel_types = fuel_types;
            return View();
        }

        [HttpGet("AllFuelTypes")]
        public IActionResult AllFuelTypes()
        {
            ViewBag.title = "Fuel Types";
            return View();
        }

        [HttpPost("CreateFuelType")]
        public IActionResult CreateCurrency(EFuelTypes type)
        {
            try
            {
                db.EFuelTypes.Add(type);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Created Fuel Type";
                return RedirectToAction("AllFuelTypes");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllFuelTypes");
            }
        }

        [HttpPost("DeleteFuelType")]
        public IActionResult DeleteFuelType(int id)
        {
            try
            {
                var type = db.EFuelTypes.Find(id);
                db.EFuelTypes.Remove(type);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("AllFuelTypes");
            }
            catch(Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] =ex.Message;
                return RedirectToAction("AllFuelTypes");
            }
        }

    }
}
