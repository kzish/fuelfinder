﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminPortal.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AdminPortal.Controllers
{
    [Route("FuelCurrencys")]
    public class FuelCurrencysController : Controller
    {

        dbContext db = new dbContext();


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [HttpGet("ajaxAllCurrencys")]
        public IActionResult ajaxAllCurrencys()
        {
            var currencys = db.EFuelCurrency.ToList();
            ViewBag.currencys = currencys;
            return View();
        }

        [HttpGet("AllCurrencys")]
        public IActionResult AllCurrencys()
        {
            ViewBag.title = "Currencies";
            return View();
        }

        [HttpPost("CreateCurrency")]
        public IActionResult CreateCurrency(EFuelCurrency _currency)
        {
            try
            {
                db.EFuelCurrency.Add(_currency);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Created Currency";
                return RedirectToAction("AllCurrencys");
            }
            catch (Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllCurrencys");
            }
        }

        [HttpPost("DeleteCurrency")]
        public IActionResult DeleteCurrency(int id)
        {
            try
            {
                var currency = db.EFuelCurrency.Find(id);
                db.EFuelCurrency.Remove(currency);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("AllCurrencys");
            }
            catch(Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] =ex.Message;
                return RedirectToAction("AllCurrencys");
            }
        }

    }
}
