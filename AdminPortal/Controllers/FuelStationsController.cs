﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminPortal.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace AdminPortal.Controllers
{
    [Route("FuelStations")]
    [Authorize(Roles = "admin")]
    public class FuelStationsController : Controller
    {
        dbContext db = new dbContext();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

        [Route("AllStations")]
        public IActionResult AllStations()
        {
            ViewBag.title = "All Stations";
            return View();
        }


        [Route("ajaxAllStations")]
        public IActionResult ajaxAllStations(string province, string city)
        {
            var stations = db.MFuelStations.AsQueryable();
            if (!String.IsNullOrEmpty(province)) stations = stations.Where(i => i.AddressProvince.Contains(province));
            if (!String.IsNullOrEmpty(city)) stations = stations.Where(i => i.AddressCity.Contains(city));
            ViewBag.stations = stations.OrderBy(i => i.StationName).ToList();
            return View();
        }

        [HttpGet("CreateStation")]
        public IActionResult CreateStation()
        {
            ViewBag.title = "Create Station";
            return View();
        }

        [HttpPost("CreateStation")]
        public IActionResult CreateStation(MFuelStations station)
        {
            ViewBag.title = "Create Station";
            try
            {
                db.MFuelStations.Add(station);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("AllStations");
            }
            catch(Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllStations");
            }
        }

        


        [HttpGet("ViewStation/{id}")]
        public IActionResult ViewStation(int id)
        {
            ViewBag.title = "View Station";
            var station = db.MFuelStations.Find(id);
            ViewBag.station = station;
            return View();
        }

        [HttpPost("UpdateStation")]
        public   IActionResult UpdateStation(MFuelStations station)
        {
            try
            {
                db.Entry(station).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                return RedirectToAction("AllStations");
            }
            catch(Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllStations");
            }
        }

        [HttpPost("DeleteStation")]
        public IActionResult DeleteStation(int id)
        {
            try
            {
                var station = db.MFuelStations.Find(id);
                db.MFuelStations.Remove(station);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Deleted";
                return RedirectToAction("AllStations");
            }
            catch(Exception ex)
            {
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
                return RedirectToAction("AllStations");
            }
        }

    }
}
