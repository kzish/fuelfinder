﻿using System;
using System.Collections.Generic;

namespace AdminPortal.Models
{
    public partial class EFuelCurrency
    {
        public EFuelCurrency()
        {
            MFuelTanks = new HashSet<MFuelTanks>();
        }

        public int Id { get; set; }
        public string Currency { get; set; }

        public virtual ICollection<MFuelTanks> MFuelTanks { get; set; }
    }
}
