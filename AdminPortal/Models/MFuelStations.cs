﻿using System;
using System.Collections.Generic;

namespace AdminPortal.Models
{
    public partial class MFuelStations
    {
        public MFuelStations()
        {
            MFuelTanks = new HashSet<MFuelTanks>();
        }

        public int Id { get; set; }
        public string StationName { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonMobile { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string AddressProvince { get; set; }
        public string AddressCity { get; set; }
        public string AddressStreet { get; set; }
        public string AddressDescription { get; set; }

        public virtual ICollection<MFuelTanks> MFuelTanks { get; set; }
    }
}
