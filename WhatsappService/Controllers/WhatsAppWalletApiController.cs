﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Menus;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WhatsappService.Models;

namespace WalletApi.Controllers
{
    /// <summary>
    /// this is a proxy class that will send messages on the selected platforms
    /// </summary>
    public class WhatsAppWalletApiController : Controller
    {
        //1=telegram
        //2=gupshup
        private static int platform = 2;


        private static string start_text = "#";//start text


        //fetch the session data
        public static MSession GetSessionData(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {
                var session_data = db.MSession
                       .Where(i => i.WhatsAppMobileNumber == message.from)
                       .FirstOrDefault();

                if (session_data == null)
                {
                    //if no session exists create new session and save it
                    session_data = new MSession();
                    session_data.WhatsAppMobileNumber = message.from;
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main";
                    session_data.IndexCounter = 0;
                    db.MSession.Add(session_data);
                    db.SaveChanges();
                }

                return session_data;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                db.Dispose();
            }
        }

        //clear my session to start afresh
        private static void ClearMySession(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var session_data = db.MSession
                    .Where(i => i.WhatsAppMobileNumber == message.from)
                    .FirstOrDefault();

                if (session_data != null)
                {
                    db.MSession.Remove(session_data);//remove old session to clear session
                }
                //add new blank session
                var new_session = new MSession();
                new_session.WhatsAppMobileNumber = message.from;
                new_session.Date = DateTime.Now;
                new_session.LastMenu = "main";
                new_session.IndexCounter = 0;
                db.MSession.Add(new_session);
                //
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                db.Dispose();
            }
        }

        //receive message callback and executes the menu
        public static void RecieveMessageCallBack(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            try
            {

                //start_text / clear session
                if (message.content.ToLower() == start_text)
                    ClearMySession(message);

                //get session
                var session_data = GetSessionData(message);

                //main menu
                if (session_data.LastMenu.Contains("main") && (message.content.ToLower() == "#" || message.content.ToLower() == start_text))
                    MenuMain.Menu1(message);//Menu main

                //option 1
                else if (session_data.LastMenu == "main" && message.content == "1")
                    FindFuelNearMe.Menu1(message);//list the currencies
                else if (session_data.LastMenu == "main.1" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                    FindFuelNearMe.Menu1(message);//navigate the currencies
                else if (session_data.LastMenu == "main.1" && message.content.All(Char.IsDigit))
                    FindFuelNearMe.Menu2(message);//accept the selected currency and prompt next menu
                else if (session_data.LastMenu == "main.1.selected_currency_id.select_fuel_type" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                    FindFuelNearMe.Menu3(message);//navigate select fuel type
                else if (session_data.LastMenu == "main.1.selected_currency_id.select_fuel_type" && message.content.All(Char.IsDigit))
                    FindFuelNearMe.Menu4(message);//accept selected fuel type and prompt send location
                else if (session_data.LastMenu == "main.1.selected_currency_id.select_fuel_type.selected_fuel_type_id" && message.type.ToLower() == "location")
                    FindFuelNearMe.Menu5(message);//accept the location and go to menu 6
                else if (session_data.LastMenu == "main.1.selected_currency_id.select_fuel_type.selected_fuel_type_id.sent_location" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                    FindFuelNearMe.Menu6(message);//navigate the filtered stations inlcuding location



                //option 2
                else if (session_data.LastMenu == "main" && message.content == "2")
                    FindFuelByCurrency.Menu1(message);//list the currencies
                else if (session_data.LastMenu == "main.2" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                    FindFuelByCurrency.Menu1(message);//navigate the currencies
                else if (session_data.LastMenu == "main.2" && message.content.All(Char.IsDigit))
                    FindFuelByCurrency.Menu2(message);//accept the selected currency and prompt next menu
                else if (session_data.LastMenu == "main.2.selected_currency_id.select_fuel_type" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                    FindFuelByCurrency.Menu3(message);//navigate select fuel type
                else if (session_data.LastMenu == "main.2.selected_currency_id.select_fuel_type" && message.content.All(Char.IsDigit))
                    FindFuelByCurrency.Menu4(message);//accept selected fuel type
                else if (session_data.LastMenu == "main.2.selected_currency_id.select_fuel_type.selected_fuel_type_id" && (message.content.ToLower() == "n" || message.content.ToLower() == "p"))
                    FindFuelByCurrency.Menu5(message);//navigate the filtered results


                //option 2
                else if (session_data.LastMenu == "main" && message.content == "3")
                {
                    WhatsAppWalletApiController
                       .SendMessageText(message.from, $"🔥🔥🔥🔥🔥 Coming soon{Environment.NewLine}").Wait();
                    Thread.Sleep(2000);
                    MenuMain.Menu1(message);//Menu main
                }


                else //todo: //show the last successful menu
                {
                    WhatsAppWalletApiController
                        .SendMessageText(message.from, $"Invalid response, try again or enter '{start_text}' to return to main menu").Wait();
                    Thread.Sleep(2000);
                    MenuMain.Menu1(message);//Menu main
                }
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //send a text message
        public static async Task<JsonResult> SendMessageText(string mobileNumber, string message_text)
        {
            if (platform == 1)
            {
                await TelegramWalletApiController.SendMessageText(mobileNumber, message_text);
            }
            if (platform == 2)
            {
                message_text = message_text.Replace("&", "AND");//whatsapp does not like ampersand symbol
                await GupshupWalletApiController.SendMessageText(mobileNumber, message_text);
            }
            return new JsonResult(new { });
        }

        //send text message with image
        public static async Task<JsonResult> SendMessageImageAndText(string mobileNumber, string image_content, string caption)
        {
            if (platform == 1)
            {
                await TelegramWalletApiController.SendMessageImageAndText(mobileNumber, image_content, caption);
            }
            if (platform == 2)
            {
                caption = caption.Replace("&", "AND");//whatsapp does not like ampersand symbol
                await GupshupWalletApiController.SendMessageImageAndText(mobileNumber, image_content, HttpUtility.HtmlEncode(caption));
            }
            return new JsonResult(new { });
        }

        //send an error text message
        public static async Task<JsonResult> SendErrorMessageText(string mobileNumber, string message_text)
        {
            //if (platform == 1) await TelegramWalletApiController.SendErrorMessageText(mobileNumber, message_text);
            //if (platform == 2) await GupshupWalletApiController.SendErrorMessageText(mobileNumber, message_text);
            //send errot through telegram always
            await TelegramWalletApiController.SendErrorMessageText("586097924", "Fuel Finder: " + message_text);
            return new JsonResult(new { });
        }




    }
}
