﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// a standard callback that is generated when an incoming message has been received by the platform
    /// app user sends a message to the platform
    /// </summary>
    public class RecieveMessageCallBack
    {
        public string content { get; set; }
        public string from { get; set; }
        public string type { get; set; }
        public Location location { get; set; } = new Location();

    }

    public class Location
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
    }
}

