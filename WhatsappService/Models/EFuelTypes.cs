﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class EFuelTypes
    {
        public EFuelTypes()
        {
            MFuelTanks = new HashSet<MFuelTanks>();
        }

        public int Id { get; set; }
        public string FuelType { get; set; }

        public virtual ICollection<MFuelTanks> MFuelTanks { get; set; }
    }
}
