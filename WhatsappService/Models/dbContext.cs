﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WhatsappService.Models
{
    public partial class dbContext : DbContext
    {
        public dbContext()
        {
        }

        public dbContext(DbContextOptions<dbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<EFuelCurrency> EFuelCurrency { get; set; }
        public virtual DbSet<EFuelTypes> EFuelTypes { get; set; }
        public virtual DbSet<MFuelStations> MFuelStations { get; set; }
        public virtual DbSet<MFuelTanks> MFuelTanks { get; set; }
        public virtual DbSet<MSession> MSession { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                //optionsBuilder.UseSqlServer("server=localhost;database=FuelFinder;User Id=sa;Password=abc123!;");
                optionsBuilder.UseSqlServer(@"server=.\SQLEXPRESS;database=FuelFinder;User Id=sa;Password=gZi3hg8dn210Q;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedUserName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<EFuelCurrency>(entity =>
            {
                entity.ToTable("eFuelCurrency");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Currency)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EFuelTypes>(entity =>
            {
                entity.ToTable("eFuelTypes");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FuelType)
                    .IsRequired()
                    .HasColumnName("fuel_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MFuelStations>(entity =>
            {
                entity.ToTable("mFuelStations");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddressCity)
                    .HasColumnName("address_city")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressDescription)
                    .HasColumnName("address_description")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressProvince)
                    .HasColumnName("address_province")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressStreet)
                    .HasColumnName("address_street")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonMobile)
                    .HasColumnName("contact_person_mobile")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersonName)
                    .HasColumnName("contact_person_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Latitude)
                    .HasColumnName("latitude")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .HasColumnName("longitude")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StationName)
                    .HasColumnName("station_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MFuelTanks>(entity =>
            {
                entity.ToTable("mFuelTanks");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.EFuelCurrency).HasColumnName("e_fuel_currency");

                entity.Property(e => e.EFuelType).HasColumnName("e_fuel_type");

                entity.Property(e => e.FuelStation).HasColumnName("fuel_station");

                entity.Property(e => e.LitresAvailable)
                    .HasColumnName("litres_available")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PricePerLitre)
                    .HasColumnName("price_per_litre")
                    .HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.EFuelCurrencyNavigation)
                    .WithMany(p => p.MFuelTanks)
                    .HasForeignKey(d => d.EFuelCurrency)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_mFuelTanks_mFuelTanks");

                entity.HasOne(d => d.EFuelTypeNavigation)
                    .WithMany(p => p.MFuelTanks)
                    .HasForeignKey(d => d.EFuelType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_mFuelTanks_eFuelTypes");

                entity.HasOne(d => d.FuelStationNavigation)
                    .WithMany(p => p.MFuelTanks)
                    .HasForeignKey(d => d.FuelStation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_mFuelTanks_mFuelStations");
            });

            modelBuilder.Entity<MSession>(entity =>
            {
                entity.HasKey(e => e.WhatsAppMobileNumber);

                entity.ToTable("mSession");

                entity.Property(e => e.WhatsAppMobileNumber)
                    .HasColumnName("whats_app_mobile_number")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.IndexCounter).HasColumnName("index_counter");

                entity.Property(e => e.LastMenu)
                    .IsRequired()
                    .HasColumnName("last_menu")
                    .IsUnicode(false);
            });
        }
    }
}
