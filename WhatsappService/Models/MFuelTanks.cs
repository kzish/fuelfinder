﻿using System;
using System.Collections.Generic;

namespace WhatsappService.Models
{
    public partial class MFuelTanks
    {
        public int Id { get; set; }
        public int EFuelType { get; set; }
        public decimal LitresAvailable { get; set; }
        public int FuelStation { get; set; }
        public decimal PricePerLitre { get; set; }
        public int EFuelCurrency { get; set; }

        public virtual EFuelCurrency EFuelCurrencyNavigation { get; set; }
        public virtual EFuelTypes EFuelTypeNavigation { get; set; }
        public virtual MFuelStations FuelStationNavigation { get; set; }
    }
}
