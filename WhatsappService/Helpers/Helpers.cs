﻿using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WhatsappService.Models;

public class Helpers
{
    public static HostingEnvironment host;

    public static string ImageToBase64(string path)
    {
        var base64String = string.Empty;
        using (Image image = Image.FromFile(path))
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat);
                byte[] imageBytes = ms.ToArray();
                base64String = Convert.ToBase64String(imageBytes);
                ms.Dispose();
            }
            image.Dispose();
        }
        return base64String;
    }


    /// <summary>
    /// return a dynamic object of the session_data.Data
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public static dynamic ReadSessionData(RecieveMessageCallBack message)
    {
        var db = new dbContext();
        try
        {
            var session_data = db.MSession.Find(message.from);
            dynamic data = JsonConvert.DeserializeObject(session_data.Data);
            return data;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            db.Dispose();
        }
    }

    /// <summary>
    /// inserts or updates a key value pair into the session_data.Data
    /// </summary>
    /// <param name="message"></param>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public static bool WriteSessionData(RecieveMessageCallBack message, string key, string value)
    {
        var db = new dbContext();
        try
        {
            var session_data = db.MSession.Find(message.from);
            dynamic data = null;
            //
            if (!string.IsNullOrEmpty(session_data.Data))
                data = JsonConvert.DeserializeObject(session_data.Data);//get the json data
            else
                data = new JObject();
            //
            data[key] = value;//add the key and value
            session_data.Data = JsonConvert.SerializeObject(data);//convert back to string
            db.Entry(session_data).State = EntityState.Modified;//save the session entry
            db.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            db.Dispose();
        }
    }


    public static bool ExecuteEcocashPayment(string phone_number, decimal amount)
    {
        //return true;
        try
        {
            //execute ecocash payment request
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            var string_content_string = $@"{{
                                        ""username"": ""{Globals.ecocash_username}"",
                                        ""password"": ""{Globals.ecocash_password}""
                                    }}";
            var stringContent = new StringContent(string_content_string, Encoding.UTF8, "application/json");

            string json = httpClient.PostAsync($"{Globals.smartpay_end_point}/api/users/authenticate", stringContent)
                .Result
                .Content
                .ReadAsStringAsync()
                .Result;
            dynamic json_result = JsonConvert.DeserializeObject(json);

            string auth_token = json_result.token;
            httpClient.DefaultRequestHeaders.Clear();
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {auth_token}");

            var response = httpClient
                .GetAsync($"{Globals.smartpay_end_point}/api/ecocash/pay?IntergrationId=1&phonenumber={phone_number}&reference={Guid.NewGuid().ToString()}&amount={amount}")
                .Result;


            var json_response_string = response
                .Content
                .ReadAsStringAsync()
                .Result;

            dynamic jresponse = JsonConvert.DeserializeObject(json_response_string);
            string StatusCode = jresponse.StatusCode;
            if (StatusCode == "1") return true;
            else return false;
        }
        catch (Exception ex)
        {
            WhatsAppWalletApiController.SendErrorMessageText("", ex.Message).Wait();
            return false;
        }
    }




}
