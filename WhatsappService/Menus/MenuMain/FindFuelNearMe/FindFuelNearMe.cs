﻿using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WhatsappService.Models;

namespace Menus
{
    public class FindFuelNearMe
    {

        private static int array_limit = 5;

        //display and navigate the currencies
        public static void Menu1(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var session_data = db.MSession.Find(message.from);
                var currencys = db.EFuelCurrency.ToList();
                int pages = currencys.Count() / array_limit;
                if (message.content.ToLower() == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
                if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev

                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/FindFuelNearMe/menu1.txt");

                //pagination
                var sub_menu = currencys
                    .Skip(array_limit * session_data.IndexCounter)
                    .Take(array_limit)
                    .ToList();

                var list = string.Empty;
                int index = (array_limit * session_data.IndexCounter);
                foreach (var c in sub_menu)
                {
                    index++;
                    list += $"*{index}* - {c.Currency}{Environment.NewLine}";
                }

                menu = menu.Replace("{{list}}", list);
                menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;

                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.1";
                db.SaveChanges();//update session
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }


        //accept the selected currency and prompt the next menu
        public static void Menu2(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                //
                var currencys = db.EFuelCurrency.ToList();
                int selected_currency_index = int.Parse(message.content);
                //invalid selection
                if (selected_currency_index <= 0 || selected_currency_index > currencys.Count)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Selection must be between 1 and {currencys.Count}");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    message.content = "";
                    Menu1(message);
                }
                else//valid selection
                {
                    var selected_currency = currencys[selected_currency_index - 1];
                    //save selection
                    Helpers.WriteSessionData(message, "selected_currency_id", selected_currency.Id.ToString());

                    var session_data = db.MSession.Find(message.from);
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main.1.selected_currency_id";
                    db.SaveChanges();//update session
                    db.Dispose();
                    //go to menu 3 navigate select fue type
                    message.content = "";
                    Menu3(message);
                }

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }


        //navigate select fuel type
        public static void Menu3(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var session_data = db.MSession.Find(message.from);
                var fuel_types = db.EFuelTypes.ToList();
                int pages = fuel_types.Count() / array_limit;
                if (message.content.ToLower() == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
                if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev

                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/FindFuelNearMe/menu3.txt");

                //pagination
                var sub_menu = fuel_types
                    .Skip(array_limit * session_data.IndexCounter)
                    .Take(array_limit)
                    .ToList();

                var list = string.Empty;
                int index = (array_limit * session_data.IndexCounter);
                foreach (var c in sub_menu)
                {
                    index++;
                    list += $"*{index}* - {c.FuelType}{Environment.NewLine}";
                }

                menu = menu.Replace("{{list}}", list);
                menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;

                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.1.selected_currency_id.select_fuel_type";
                db.SaveChanges();//update session
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }


        //accept the selected fuel type
        //prompt send location
        public static void Menu4(RecieveMessageCallBack message)
        {
            var db = new dbContext();
            var menu = string.Empty;
            try
            {
                //
                var fuel_types = db.EFuelTypes.ToList();
                int selected_fuel_type_index = int.Parse(message.content);
                //invalid selection
                if (selected_fuel_type_index <= 0 || selected_fuel_type_index > fuel_types.Count)
                {
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/error_message.txt");
                    menu = menu.Replace("{{error_message}}", $"Selection must be between 1 and {fuel_types.Count}");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                    //prev menu
                    message.content = "";
                    Menu3(message);
                }
                else//valid selection
                {
                    var selected_fuel_type = fuel_types[selected_fuel_type_index - 1];
                    //save selection
                    Helpers.WriteSessionData(message, "selected_fuel_type_id", selected_fuel_type.Id.ToString());

                    var session_data = db.MSession.Find(message.from);
                    session_data.Date = DateTime.Now;
                    session_data.LastMenu = "main.1.selected_currency_id.select_fuel_type.selected_fuel_type_id";
                    db.SaveChanges();//update session
                                     //prompt send location
                    menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/FindFuelNearMe/menu4.txt");
                    WhatsAppWalletApiController.SendMessageText(message.from, menu).Wait();
                }

            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //accept the location
        //and go to menu 6
        public static void Menu5(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                Helpers.WriteSessionData(message, "latitude", message.location.latitude);
                Helpers.WriteSessionData(message, "longitude", message.location.longitude);
                message.content = "";
                Menu6(message);//go to menu 6
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }

        //navigate filters
        public static void Menu6(RecieveMessageCallBack message)
        {
            var db = new dbContext();

            try
            {
                var session_data = db.MSession.Find(message.from);
                dynamic session_data_data = Helpers.ReadSessionData(message);

                int selected_fuel_type_id = int.Parse($"{session_data_data.selected_fuel_type_id}");
                int selected_currency_id = int.Parse($"{session_data_data.selected_currency_id}");
                decimal latitude = decimal.Parse($"{session_data_data.latitude}");
                decimal longitude = decimal.Parse($"{session_data_data.longitude}");

                var tanks = db.MFuelTanks
                    .Where(i => i.EFuelCurrency == selected_currency_id)
                    .Where(i => i.EFuelType == selected_fuel_type_id)
                    .Include(i => i.FuelStationNavigation)
                    .ToList();

                int pages = tanks.Count() / array_limit;
                if (message.content.ToLower() == "n" && (session_data.IndexCounter < pages)) session_data.IndexCounter++;//go next
                if (message.content.ToLower() == "p" && (session_data.IndexCounter >= 1)) session_data.IndexCounter--;//go prev

                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/FindFuelNearMe/menu6.txt");

                //pagination
                var sub_menu = tanks
                    .Skip(array_limit * session_data.IndexCounter)
                    .Take(array_limit)
                    .ToList();

                var list = string.Empty;
                int index = (array_limit * session_data.IndexCounter);
                foreach (var c in sub_menu)
                {
                    index++;
                    list += $"*{index}* - {c.FuelStationNavigation.StationName}{Environment.NewLine}";
                    list += $"_remaining litres_........*{c.LitresAvailable.ToString("0.00")}*{Environment.NewLine}";
                    list += $"_price_........*{c.PricePerLitre.ToString("0.00")}*{Environment.NewLine}";
                    list += $"*_{c.FuelStationNavigation.AddressProvince}|{c.FuelStationNavigation.AddressCity}_*{Environment.NewLine}";
                    list += $"_{c.FuelStationNavigation.AddressDescription}_{Environment.NewLine}{Environment.NewLine}";
                }
                var currency = db.EFuelCurrency.Find(selected_currency_id);
                var fuel_type = db.EFuelTypes.Find(selected_fuel_type_id);

                menu = menu.Replace("{{currency}}", currency.Currency);
                menu = menu.Replace("{{fuel_type}}", fuel_type.FuelType);
                menu = menu.Replace("{{list}}", list);
                menu = menu.Replace("{{pager}}", $"{session_data.IndexCounter + 1} of {pages + 1}");
                var response = WhatsAppWalletApiController.SendMessageText(message.from, menu).Result;

                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main.1.selected_currency_id.select_fuel_type.selected_fuel_type_id.sent_location";
                db.SaveChanges();//update session
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from, ex.Message).Wait();
            }
            finally
            {
                db.Dispose();
            }
        }




    }
}


