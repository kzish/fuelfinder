﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalletApi.Controllers;
using WhatsappService.Models;

namespace Menus
{
    public class MenuMain
    {
        public static void Menu1(RecieveMessageCallBack message)
        {
            try
            {
                var db = new dbContext();
                var menu = System.IO.File.ReadAllText(Helpers.host.WebRootPath + "/Menus/MenuMain/menu_main.txt");
                var image_path = $"{Globals.base_path}/Raw/fuel_icon.jpg";
                //send
                WhatsAppWalletApiController.SendMessageImageAndText(message.from,image_path, menu).Wait();
                //
                var session_data = db.MSession.Find(message.from);
                session_data.Date = DateTime.Now;
                session_data.LastMenu = "main";
                session_data.IndexCounter = 0;//reset indexCounter
                session_data.Data = null;
                db.SaveChanges();//update session
                db.Dispose();
            }
            catch (Exception ex)
            {
                WhatsAppWalletApiController.SendErrorMessageText(message.from,"line 33 MenuMain.Menu1"+ ex.Message).Wait();
            }
        }

    }
}
